const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Album = require("../models/Album");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const query = {};
        let result;
        if (req.query.artist) {
            query.artist = req.query.artist;
            try {
                result = await Album.find({artist: query.artist}).sort({releaseDate: 1});
            } catch (e) {
                return res.status(404).send({message: e.message});
            }
        } else {
            result = await Album.find(query).populate("artist", "name info photo");
        }
        if (!result || result.length === 0) {
            return res.status(404).send({message: "Not found"});
        }
        res.status(200).send(result);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const album =  await Album.findById(req.params.id).populate("artist", "name info photo");
        if (!album || album.length === 0) {
            return res.status(400).send({message: "Not found"});
        } else {
            res.send(album);
        }
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', [auth, upload.single('image')], async (req, res) => {
    if (!req.body.name || req.body.name === "") {
        return res.status(400).send({message: "Data not valid!"});
    }
    if (!req.body.artist || req.body.artist === "") {
        return res.status(400).send({message: "Data not valid!"});
    }

    const albumData = {
        name: req.body.name,
        artist: req.body.artist,
        releaseDate: req.body.releaseDate
    };

    if (req.file) {
        albumData.image = `uploads/${req.file.filename}`;
    }

    if (req.releaseDate) {
        try {
            albumData.releaseDate = new Date(req.releaseDate);
        } catch (e) {
            return res.status(500).send({message: e});
        }
    }

    const album = new Album(albumData);

    try {
        await album.save();
        res.status(200).send(album);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.delete("/:id", [auth, permit("admin")], async (req, res) => {
    try {
        const item = await Album.findOneAndDelete({_id: req.params.id});
        return res.send(item);
    } catch (e) {
        res.status(500).send({message: e});
    }
});

router.post("/:id/publish", [auth, permit("admin")], async (req, res) => {
    try {
        const item = await Album.findOne({_id: req.params.id});

        if (item.published === true) {
            return res.status(400).send({message: "It's already published!"});
        }

        item.published = true;
        item.save();
        return res.send(item);
    } catch (e) {
        return res.status(400).send({message: e});
    }
});

module.exports = router;