const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Artist = require("../models/Artist");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const result = await Artist.find();
        if (!result || result.length === 0) {
            return res.status(404).send({message: "Not found"});
        }
        res.status(200).send(result);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const result = await Artist.findOne({_id: req.params.id});
        if (!result) {
            return res.status(404).send({message: "Not found"});
        }
        res.status(200).send(result);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', [auth, upload.single('photo')], async (req, res) => {
    try {
        if (!req.body.name) {
            return res.status(400).send({message: "Data not valid!"});
        }
        const artistData = {
            name: req.body.name,
            info: req.body.info ? req.body.info : null,
        };

        if (req.file) {
            artistData.photo = `uploads/${req.file.filename}`;
        }

        const artist = new Artist(artistData);
        await artist.save();
        return res.status(200).send(artist);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.post("/:id/publish", [auth, permit("admin")], async (req, res) => {
    try {
        const item = await Artist.findOne({_id: req.params.id});

        if (item.published === true) {
            return res.status(400).send({message: "It's already published!"});
        }

        item.published = true;
        item.save();
        return res.send(item);
    } catch (e) {
        return  res.status(400).send({message: e});
    }
});

router.delete("/:id", [auth, permit("admin")], async (req, res) => {
    try {
        const item = await Artist.findOneAndDelete({_id: req.params.id});
        return res.send(item);
    } catch (e) {
        res.status(500).send({message: e});
    }
});


module.exports = router;