const express = require('express');
const Track = require("../models/Track");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const query = {};
        let result;
        if (req.query.album) {
            query.album = req.query.album;
            try {
                result = await Track.find({album: query.album}).sort({trackNumber: 1});
            } catch (e) {
                return res.status(404).send({message: e.message});
            }
        } else {
            result = await Track.find(query).populate("album", "name date image");
        }
        if (!result || result.length === 0) {
            return res.status(404).send({message: "Not found"});
        }
        res.status(200).send(result);
    } catch (e) {
        return res.status(500).send({message: e});
    }
});

router.post('/', auth, async (req, res) => {
    try {
        console.log(req.body);
        const trackData = {
            name: req.body.name,
            duration: req.body.duration,
            album: req.body.album,
            trackNumber: req.body.trackNumber,
        };

        const track = new Track(trackData);

        await track.save();
        return res.status(200).send(track);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.delete("/:id", [auth, permit("admin")], async (req, res) => {
    try {
        const item = await Track.findOneAndDelete({_id: req.params.id});
        return res.send(item);
    } catch (e) {
        res.status(500).send({message: e});
    }
});

router.post("/:id/publish", [auth, permit("admin")], async (req, res) => {
    try {
        const item = await Track.findOne({_id: req.params.id});

        if (item.published === true) {
            return res.status(400).send({message: "It's already published!"});
        }

        item.published = true;
        item.save();
        return res.send(item);
    } catch (e) {
        return res.status(400).send({message: e});
    }
});


module.exports = router;