const express = require('express');
const Track = require('../models/Track');
const TrackHistory = require('../models/TrackHistory');
const auth = require('../middleware/auth');

const router = express.Router();


router.get('/track_history', auth, async (req, res) => {
    try {
        const result = await TrackHistory.find({user: req.user._id})
            .populate({
                path: "track",
                select: "name",
                populate: {
                    path: "album",
                    select: "name",
                    populate: {
                        path: "artist",
                        select: "name"
                    }
                }
            }).sort({datetime: -1});
        res.send(result);
    } catch (e) {
        return res.status(404).send({message: e.message});
    }
});

router.post('/track_history', auth, async (req, res) => {
    if (!req.body.track_id) {
        return res.status(400).send({message: "Invalid data"});
    }
    let track;
    try {
        track = await Track.findOne({_id: req.body.track_id});
    } catch (e) {
        return res.status(400).send({message: "Wrong track ID"});
    }

    if (!track) {
        return res.status(404).send({message: "Not found"});
    }

    const historyData = {
        track: track,
        user: req.user.id,
    };

    try {
        const trackHistoryObj = new TrackHistory(historyData);
        await trackHistoryObj.save();
        return res.send(trackHistoryObj);
    } catch (e) {
        return res.status(500).send(e);
    }
});


module.exports = router;