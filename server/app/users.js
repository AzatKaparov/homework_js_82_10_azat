const express = require('express');
const User = require('../models/User');
const axios = require('axios');
const config = require('../config');
const {nanoid} = require('nanoid');

const router = express.Router();

router.post('/', async(req, res) => {
    try {
        const userData = {
            username: req.body.username,
            password: req.body.password,
        };

        const user = new User(userData);
        user.generateToken();
        await user.save();
        res.send(user);
    } catch (e) {
        return res.status(401).send({message: "Something went wrong"});
    }
});

router.post('/sessions', async (req, res) => {
    try {
        const user = await User.findOne({username: req.body.username});

        if (!user) {
            return res.status(401).send({message: 'Credentials are wrong!'});
        }

        const isMatch = await user.checkPassword(req.body.password);

        if (!isMatch) {
            return res.status(401).send({message: 'Credentials are wrong!'});
        }

        user.generateToken();
        await user.save({validateBeforeSave: false});

        res.send({message: 'Username and password correct!', user});
    } catch (e) {
        return res.status(500).send(e);
    }
});

router.delete("/sessions", async (req, res) => {
    try {
        const token = req.get('Authorization');
        const success = {message: 'Success'};

        if (!token) return res.send(success);

        const user = await User.findOne({token});

        if (!user) return res.send(success);

        user.token = '';
        user.save({validateBeforeSave: false});

        return res.send(success);
    } catch (e) {
        return res.status(500).send(e);
    }
});

router.post('/facebookLogin', async (req, res) => {
    const inputToken = req.body.accessToken;

    const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;

    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

    try {
        const response = await axios.get(debugTokenUrl);

        if (response.data.data.error) {
            return res.status(401).send({message: 'Facebook token incorrect'});
        }

        if (req.body.id !== response.data.data.user_id) {
            return res.status(401).send({message: 'Wrong User Id'});
        }

        let user = await User.findOne({username: req.body.email});

        console.log(response.data);


        if (!user) {
            user = new User({
                username: req.body.email || nanoid(),
                password: nanoid(),
                displayName: req.body.name,
                avatarImage: req.body.picture.data.url,
            });
        }

        user.generateToken();
        user.save({validateBeforeSave: false});

        res.send({message: 'Success', user});
    } catch (e) {
        res.status(401).send({global: 'Facebook token incorrect!'});
    }
});


module.exports = router;