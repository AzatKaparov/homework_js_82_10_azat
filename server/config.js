const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    db: {
        url: "mongodb://localhost/music"
    },
    facebook: {
        appId: 1059988424571925,
        appSecret: "23fef8a6bd6b4c59cd95cd6c83c9a02a",
    },
};