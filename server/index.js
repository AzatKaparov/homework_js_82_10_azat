const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const artist = require('./app/artist');
const albums = require('./app/album');
const track = require('./app/track');
const users = require('./app/users');
const trackHistory = require('./app/trackHistory');

const app = express();
const port = 8000;

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

app.use('/artists', artist);
app.use('/albums', albums);
app.use('/tracks', track);
app.use('/users', users);
app.use('/', trackHistory);

const run = async () => {
    await mongoose.connect('mongodb://localhost/music');

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    process.on('exit', async () => {
        console.log('exiting');
        await mongoose.disconnect();
    });
};

run().catch(e => console.error(e));