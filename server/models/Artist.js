const mongoose = require('mongoose');
const Shema = mongoose.Schema;

const ArtistShema = new Shema({
    name: {
        type: String,
        required: true
    },
    info: {
        type: String,
    },
    photo: {
        type: String,
    },
    published: {
        type: Boolean,
        required: true,
        default: false
    }
});

const Artist = mongoose.model('Artist', ArtistShema);
module.exports = Artist;