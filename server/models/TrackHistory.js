const mongoose = require('mongoose');
const Shema = mongoose.Schema;

const TrackHistoryShema = new Shema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    track: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Track',
        required: true,
    },
    datetime: {
        type: Date,
        required: true,
        default: () => Date.now() + 7*24*60*60*1000,
    }
});


const TrackHistory = mongoose.model('TrackHistory', TrackHistoryShema);
module.exports = TrackHistory;