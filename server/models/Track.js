const mongoose = require('mongoose');
const Shema = mongoose.Schema;

const TrackShema = new Shema({
    name: {
        type: String,
        required: true,
    },
    album: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Album',
        required: true,
    },
    duration: {
        type: String,
        required: true,
    },
    trackNumber: {
        type: Number,
        required: true,
    },
    published: {
        type: Boolean,
        required: true,
        default: false
    }
});

const Track = mongoose.model('Track', TrackShema);
module.exports = Track;