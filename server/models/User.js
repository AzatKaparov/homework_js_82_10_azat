const mongoose = require('mongoose');
const Shema = mongoose.Schema;
const bcrypt = require('bcrypt');
const {nanoid} = require('nanoid');


const SALT_WORK_FACTOR = 10;

const UserShema = new Shema({
    username: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: async value => {
                const user = await User.findOne({username: value});
            },
            message: "This user already registered"
        }
    },
    password: {
        type: String,
        required: true,
    },
    token: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        required: true,
        default: "user",
        enum: ["user", "admin"],
    },
    avatarImage: {
        type: String,
    },
    displayName: {
        type: String,
        default: "Anonymous"
    },
});

UserShema.pre('save', async function (next) {
    if (!this.isModified('password')) return next();

    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    const hash = await bcrypt.hash(this.password, salt);

    this.password = hash;

    next();
});

UserShema.set('toJSON', {
    transform: (doc, ret, options) => {
        delete ret.password;
        return ret;
    }
});

UserShema.methods.checkPassword = function (password) {
    return bcrypt.compare(password, this.password);
};

UserShema.methods.generateToken = function() {
    this.token = nanoid();
};


const User = mongoose.model('User', UserShema);
module.exports = User;