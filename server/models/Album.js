const mongoose = require('mongoose');
const Shema = mongoose.Schema;

const AlbumShema = new Shema({
    name: {
        type: String,
        required: true,
    },
    image: {
        type: String,
    },
    artist: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Artist',
        required: true,
    },
    releaseDate: {
        type: Date,
        required: true,
        default: () => Date.now() + 7*24*60*60*1000,
    },
    published: {
        type: Boolean,
        required: true,
        default: false
    }
});

const Album = mongoose.model('Album', AlbumShema);
module.exports = Album;