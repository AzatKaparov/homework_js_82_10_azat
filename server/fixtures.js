const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');
const User = require("./models/User");

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }


    const [artist1, artist2] = await Artist.create({
        name: 'Juice WRLD',
        info: '999shit forever',
        photo: "fixtures/juice.jpg",
        published: true,
    }, {
        name: 'XXXTentacion',
        info: 'Kill my vibe',
        photo: "fixtures/xxxtent.jpg"
    });

    const [album1, album2, album3, album4] = await Album.create({
        name: 'Death race for love',
        artist: artist1,
        image: "fixtures/deathrace.jpg",
        published: true,
    }, {
        name: '17',
        artist: artist2,
        image: "fixtures/17.jpg",
        published: true,
    }, {
        name: "Goodbye and good riddance",
        artist: artist1,
        image: "fixtures/goodbye.jpg"
    }, {
        name: "?",
        artist: artist2,
        image: "fixtures/asd.jpg"
    });

    await Track.create({
        name: "Desire",
        album: album1,
        duration: "02:31",
        trackNumber: 1,
        published: true,
    }, {
        name: "Maze",
        album: album1,
        duration: "02:41",
        trackNumber: 2,
    }, {
        name: "For god",
        album: album1,
        duration: "01:31",
        trackNumber: 3,
    }, {
        name: "Damn",
        album: album1,
        duration: "02:11",
        trackNumber: 4,
    }, {
        name: "Freeze",
        album: album1,
        duration: "02:11",
        trackNumber: 5,
    }, {
        name: "Kill my vibe",
        album: album2,
        duration: "02:31",
        trackNumber: 1,
    }, {
        name: "Hope",
        album: album2,
        duration: "02:41",
        trackNumber: 2,
    }, {
        name: "Curse",
        album: album2,
        duration: "01:31",
        trackNumber: 3,
    }, {
        name: "17",
        album: album2,
        duration: "02:11",
        trackNumber: 4,
    }, {
        name: "Loop",
        album: album2,
        duration: "02:11",
        trackNumber: 5,
    }, {
        name: "Truth",
        album: album3,
        duration: "02:31",
        trackNumber: 1,
    }, {
        name: "Lie",
        album: album3,
        duration: "02:41",
        trackNumber: 2,
    }, {
        name: "Dying",
        album: album3,
        duration: "01:31",
        trackNumber: 3,
    }, {
        name: "Fuck that shit",
        album: album3,
        duration: "02:11",
        trackNumber: 4,
    }, {
        name: "I'm out",
        album: album3,
        duration: "02:11",
        trackNumber: 5,
    }, {
        name: "AXZ",
        album: album4,
        duration: "02:31",
        trackNumber: 1,
    }, {
        name: "Problems",
        album: album4,
        duration: "02:41",
        trackNumber: 2,
    }, {
        name: "Oh*et'",
        album: album4,
        duration: "01:31",
        trackNumber: 3,
    }, {
        name: "Cradles",
        album: album4,
        duration: "02:11",
        trackNumber: 4,
    }, {
        name: "Eagle for one",
        album: album4,
        duration: "02:11",
        trackNumber: 5,
    });

    await User.create({
        username: 'admin',
        password: 'admin',
        token: nanoid(),
        role: "admin"
    }, {
        username: 'root',
        password: 'root',
        token: nanoid(),
        role: "admin"
    }, {
        username: 'user',
        password: 'user',
        token: nanoid(),
        role: "user"
    }, {
        username: 'azat',
        password: 'azat',
        token: nanoid(),
        role: "user"
    });

    await mongoose.connection.close();
};

run().catch(console.error);