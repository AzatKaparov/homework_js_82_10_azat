import './App.css';
import {Route, Switch, Redirect} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Artists from "./containers/Artists/Artists";
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import {useSelector} from "react-redux";
import AddArtist from "./containers/AddArtist/AddArtist";
import AddAlbum from "./containers/AddAlbum/AddAlbum";
import AddTrack from "./containers/AddTrack/AddTrack";


const App = () => {
    const user = useSelector(state => state.users.user);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };

    return (
        <div className="App">
            <Layout>
                <Switch>
                    <Route path="/" exact component={Artists}/>
                    <ProtectedRoute
                        path="/artists/add"
                        component={AddArtist}
                        isAllowed={!!user}
                        redirectTo="/login"
                        exact
                    />
                    <ProtectedRoute
                        path="/albums/add"
                        component={AddAlbum}
                        isAllowed={!!user}
                        redirectTo="/login"
                        exact
                    />
                    <ProtectedRoute
                        path="/tracks/add"
                        component={AddTrack}
                        isAllowed={!!user}
                        redirectTo="/login"
                        exact
                    />
                    <Route path="/albums" exact component={Albums}/>
                    <Route path="/tracks" exact component={Tracks}/>
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                    <Route path="/track_history" exact component={TrackHistory}/>
                    <Route render={() => <h1>Not found</h1>}/>
                </Switch>
            </Layout>
        </div>
    );
};

export default App;
