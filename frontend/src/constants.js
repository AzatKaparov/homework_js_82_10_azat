export const API_URL = "http://localhost:8000";

export const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

export const transformDate = (date) => {
    const initialDate = new Date(date);
    initialDate.toLocaleString("default", {month: 'long'});
    const year = initialDate.getFullYear();
    const month = monthNames[initialDate.getMonth()];
    const day = initialDate.getUTCDate() + 1;
    return `${day} ${month} ${year}`;
};

export const facebookAppId = "1059988424571925";