import {axiosApi} from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_ALBUMS_REQUEST = "FETCH_ALBUMS_REQUEST";
export const FETCH_ALBUMS_SUCCESS = "FETCH_ALBUMS_SUCCESS";
export const FETCH_ALBUMS_FAILURE = "FETCH_ALBUMS_FAILURE";

export const fetchAlbumsRequest = () => ({type: FETCH_ALBUMS_REQUEST});
export const fetchAlbumsSuccess = albums => ({type: FETCH_ALBUMS_SUCCESS, payload: albums});
export const fetchAlbumsFailure = err => ({type: FETCH_ALBUMS_FAILURE, payload: err});

export const fetchAlbums = (artist_id) => {
    return async dispatch => {
        dispatch(fetchAlbumsRequest());

        try {
            const response = await axiosApi.get(artist_id ? `/albums?artist=${artist_id}` : "/albums");
            dispatch(fetchAlbumsSuccess(response.data));
        } catch (e) {
            dispatch(fetchAlbumsFailure(e));
        }
    };
};

export const FETCH_ALBUM_REQUEST = "FETCH_ALBUM_REQUEST";
export const FETCH_ALBUM_SUCCESS = "FETCH_ALBUM_SUCCESS";
export const FETCH_ALBUM_FAILURE = "FETCH_ALBUM_FAILURE";

export const fetchAlbumRequest = () => ({type: FETCH_ALBUM_REQUEST});
export const fetchAlbumSuccess = album => ({type: FETCH_ALBUM_SUCCESS, payload: album});
export const fetchAlbumFailure = err => ({type: FETCH_ALBUM_FAILURE, payload: err});

export const fetchAlbum = (id) => {
    return async dispatch => {
        dispatch(fetchAlbumRequest());

        try {
            const response = await axiosApi.get(`/albums/${id}`);
            dispatch(fetchAlbumSuccess(response.data));
        } catch (e) {
            dispatch(fetchAlbumFailure(e));
        }
    };
};

export const CREATE_ALBUM_REQUEST = "CREATE_ALBUM_REQUEST";
export const CREATE_ALBUM_SUCCESS = "CREATE_ALBUM_SUCCESS";
export const CREATE_ALBUM_FAILURE = "CREATE_ALBUM_FAILURE";

export const createAlbumRequest = () => ({type: CREATE_ALBUM_REQUEST});
export const createAlbumSuccess = () => ({type: CREATE_ALBUM_SUCCESS});
export const createAlbumFailure = err => ({type: CREATE_ALBUM_FAILURE, payload: err});

export const createAlbum = (productData) => {
    return async (dispatch, getState) => {
        dispatch(createAlbumRequest());

        try {
            await axiosApi.post(
                "/albums",
                productData,
                {headers: {
                        "Authorization": getState().users.user.token,
                    }}
            );
            dispatch(createAlbumSuccess());
            dispatch(historyPush(`/`));
        } catch (e) {
            dispatch(createAlbumFailure(e));
        }
    };
};


export const DELETE_ALBUM_REQUEST = "DELETE_ALBUM_REQUEST";
export const DELETE_ALBUM_SUCCESS = "DELETE_ALBUM_SUCCESS";
export const DELETE_ALBUM_FAILURE = "DELETE_ALBUM_FAILURE";

export const deleteAlbumRequest = () => ({type: DELETE_ALBUM_REQUEST});
export const deleteAlbumSuccess = album => ({type: DELETE_ALBUM_SUCCESS, payload: album});
export const deleteAlbumFailure = err => ({type: DELETE_ALBUM_FAILURE, payload: err});

export const deleteAlbum = (id) => {
    return async (dispatch, getState) => {
        dispatch(deleteAlbumRequest());

        try {
            const response = await axiosApi.delete(
                `/albums/${id}`,
                {headers: {
                        "Authorization": getState().users.user.token,
                    }}
            );
            dispatch(deleteAlbumSuccess(response.data));
        } catch (e) {
            dispatch(deleteAlbumFailure(e));
        }
    };
};