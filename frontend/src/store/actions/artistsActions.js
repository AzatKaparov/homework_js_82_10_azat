import {axiosApi} from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_ARTISTS_REQUEST = "FETCH_ARTISTS_REQUEST";
export const FETCH_ARTISTS_SUCCESS = "FETCH_ARTISTS_SUCCESS";
export const FETCH_ARTISTS_FAILURE = "FETCH_ARTISTS_FAILURE";

export const fetchArtistsRequest = () => ({type: FETCH_ARTISTS_REQUEST});
export const fetchArtistsSuccess = artists => ({type: FETCH_ARTISTS_SUCCESS, payload: artists});
export const fetchArtistsFailure = err => ({type: FETCH_ARTISTS_FAILURE, payload: err});

export const fetchArtists = () => {
    return async dispatch => {
        dispatch(fetchArtistsRequest());

        try {
            const response = await axiosApi.get(`/artists`);
            dispatch(fetchArtistsSuccess(response.data));
        } catch (e) {
            dispatch(fetchArtistsFailure(e));
        }
    };
};

export const FETCH_ARTIST_REQUEST = "FETCH_ARTIST_REQUEST";
export const FETCH_ARTIST_SUCCESS = "FETCH_ARTIST_SUCCESS";
export const FETCH_ARTIST_FAILURE = "FETCH_ARTIST_FAILURE";

export const fetchArtistRequest = () => ({type: FETCH_ARTIST_REQUEST});
export const fetchArtistSuccess = artist => ({type: FETCH_ARTIST_SUCCESS, payload: artist});
export const fetchArtistFailure = err => ({type: FETCH_ARTIST_FAILURE, payload: err});

export const fetchArtist = (id) => {
    return async dispatch => {
        dispatch(fetchArtistRequest());

        try {
            const response = await axiosApi.get(`/artists/${id}`);
            dispatch(fetchArtistSuccess(response.data));
        } catch (e) {
            dispatch(fetchArtistFailure(e));
        }
    };
};


export const CREATE_ARTIST_REQUEST = "CREATE_ARTIST_REQUEST";
export const CREATE_ARTIST_SUCCESS = "CREATE_ARTIST_SUCCESS";
export const CREATE_ARTIST_FAILURE = "CREATE_ARTIST_FAILURE";

export const createArtistRequest = () => ({type: CREATE_ARTIST_REQUEST});
export const createArtistSuccess = artist => ({type: CREATE_ARTIST_SUCCESS, payload: artist});
export const createArtistFailure = err => ({type: CREATE_ARTIST_FAILURE, payload: err});

export const createArtist = (productData) => {
    return async (dispatch, getState) => {
        dispatch(createArtistRequest());

        try {
            const response = await axiosApi.post(
                "/artists",
                productData,
                {headers: {
                        "Authorization": getState().users.user.token,
                    }}
            );
            dispatch(createArtistSuccess(response.data));
            dispatch(historyPush(`/`));
        } catch (e) {
            dispatch(createArtistFailure(e));
        }
    };
};


export const DELETE_ARTIST_REQUEST = "DELETE_ARTIST_REQUEST";
export const DELETE_ARTIST_SUCCESS = "DELETE_ARTIST_SUCCESS";
export const DELETE_ARTIST_FAILURE = "DELETE_ARTIST_FAILURE";

export const deleteArtistRequest = () => ({type: DELETE_ARTIST_REQUEST});
export const deleteArtistSuccess = artist => ({type: DELETE_ARTIST_SUCCESS, payload: artist});
export const deleteArtistFailure = err => ({type: DELETE_ARTIST_FAILURE, payload: err});

export const deleteArtist = (id) => {
    return async (dispatch, getState) => {
        dispatch(deleteArtistRequest());

        try {
            const response = await axiosApi.delete(
                `/artists/${id}`,
                {headers: {
                        "Authorization": getState().users.user.token,
                    }}
            );
            dispatch(deleteArtistSuccess(response.data));
        } catch (e) {
            dispatch(deleteArtistFailure(e));
        }
    };
};


export const PUBLISH_ARTIST_REQUEST = "PUBLISH_ARTIST_REQUEST";
export const PUBLISH_ARTIST_SUCCESS = "PUBLISH_ARTIST_SUCCESS";
export const PUBLISH_ARTIST_FAILURE = "PUBLISH_ARTIST_FAILURE";

export const publishArtistRequest = () => ({type: PUBLISH_ARTIST_REQUEST});
export const publishArtistSuccess = artist => ({type: PUBLISH_ARTIST_SUCCESS, payload: artist});
export const publishArtistFailure = err => ({type: PUBLISH_ARTIST_FAILURE, payload: err});

export const publishArtist = (id) => {
    return async (dispatch, getState) => {
        dispatch(publishArtistRequest());

        try {
            const response = await axiosApi.post(
                `/artists/${id}/publish`,
                {headers: {
                        "Authorization": getState().users.user.token,
                    }}
            );
            dispatch(publishArtistSuccess(response.data));
        } catch (e) {
            dispatch(publishArtistFailure(e));
        }
    };
};

