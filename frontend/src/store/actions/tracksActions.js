import {axiosApi} from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_TRACKS_REQUEST = "FETCH_TRACKS_REQUEST";
export const FETCH_TRACKS_SUCCESS = "FETCH_TRACKS_SUCCESS";
export const FETCH_TRACKS_FAILURE = "FETCH_TRACKS_FAILURE";

export const fetchTracksRequest = () => ({type: FETCH_TRACKS_REQUEST});
export const fetchTracksSuccess = tracks => ({type: FETCH_TRACKS_SUCCESS, payload: tracks});
export const fetchTracksFailure = err => ({type: FETCH_TRACKS_FAILURE, payload: err});

export const fetchTracks = (album_id) => {
    return async dispatch => {
        dispatch(fetchTracksRequest());

        try {
            const response = await axiosApi.get(`/tracks?album=${album_id}`);
            dispatch(fetchTracksSuccess([]));
            dispatch(fetchTracksSuccess(response.data));
        } catch (e) {
            dispatch(fetchTracksFailure(e));
            dispatch(fetchTracksSuccess([]));
        }
    };
};


export const FETCH_TRACK_HISTORY_REQUEST = "FETCH_TRACK_HISTORY_REQUEST";
export const FETCH_TRACK_HISTORY_SUCCESS = "FETCH_TRACK_HISTORY_SUCCESS";
export const FETCH_TRACK_HISTORY_FAILURE = "FETCH_TRACK_HISTORY_FAILURE";

export const fetchTrackHistoryRequest = () => ({type: FETCH_TRACK_HISTORY_REQUEST});
export const fetchTrackHistorySuccess = historyTracks => ({type: FETCH_TRACK_HISTORY_SUCCESS, payload: historyTracks});
export const fetchTrackHistoryFailure = err => ({type: FETCH_TRACK_HISTORY_FAILURE, payload: err});

export const fetchTrackHistory = () => {
    return async (dispatch, getState) => {
        dispatch(fetchTrackHistoryRequest());

        try {
            const response = await axiosApi.get(
                `/track_history`,
                {headers: {
                    "Authorization": getState().users.user.token,
                }});
            dispatch(fetchTrackHistorySuccess(response.data));
        } catch (e) {
            dispatch(fetchTrackHistoryFailure(e));
        }
    };
};

export const CREATE_TRACK_HISTORY_REQUEST = "CREATE_TRACK_HISTORY_REQUEST";
export const CREATE_TRACK_HISTORY_SUCCESS = "CREATE_TRACK_HISTORY_SUCCESS";
export const CREATE_TRACK_HISTORY_FAILURE = "CREATE_TRACK_HISTORY_FAILURE";

export const createTrackHistoryRequest = () => ({type: CREATE_TRACK_HISTORY_REQUEST});
export const createTrackHistorySuccess = () => ({type: CREATE_TRACK_HISTORY_SUCCESS});
export const createTrackHistoryFailure = err => ({type: CREATE_TRACK_HISTORY_FAILURE, payload: err});

export const createTrackHistory = track_id => {
    return async (dispatch, getState) => {
        dispatch(createTrackHistoryRequest());

        try {
            const data = {track_id: track_id};
            await axiosApi.post(
                '/track_history',
                data,
                {headers: {"Authorization": getState().users.user.token}});
            dispatch(createTrackHistorySuccess());
        } catch (e) {
            dispatch(createTrackHistoryFailure(e));
        }
    }
};

export const CREATE_TRACK_REQUEST = "CREATE_TRACK_REQUEST";
export const CREATE_TRACK_SUCCESS = "CREATE_TRACK_SUCCESS";
export const CREATE_TRACK_FAILURE = "CREATE_TRACK_FAILURE";

export const createTrackRequest = () => ({type: CREATE_TRACK_REQUEST});
export const createTrackSuccess = () => ({type: CREATE_TRACK_SUCCESS});
export const createTrackFailure = err => ({type: CREATE_TRACK_FAILURE, payload: err});

export const createTrack = (trackData) => {
    return async (dispatch, getState) => {
        dispatch(createTrackRequest());
        console.log(trackData);

        try {
            await axiosApi.post(
                "/tracks",
                trackData,
                {headers: {
                        "Authorization": getState().users.user.token,
                    }}
            );
            dispatch(createTrackSuccess());
            dispatch(historyPush(`/`));
        } catch (e) {
            dispatch(createTrackFailure(e));
        }
    };
};

export const DELETE_TRACK_REQUEST = "DELETE_TRACK_REQUEST";
export const DELETE_TRACK_SUCCESS = "DELETE_TRACK_SUCCESS";
export const DELETE_TRACK_FAILURE = "DELETE_TRACK_FAILURE";

export const deleteTrackRequest = () => ({type: DELETE_TRACK_REQUEST});
export const deleteTrackSuccess = track => ({type: DELETE_TRACK_SUCCESS, payload: track});
export const deleteTrackFailure = err => ({type: DELETE_TRACK_FAILURE, payload: err});

export const deleteTrack = (id) => {
    return async (dispatch, getState) => {
        dispatch(deleteTrackRequest());

        try {
            const response = await axiosApi.delete(
                `/tracks/${id}`,
                {headers: {
                        "Authorization": getState().users.user.token,
                    }}
            );
            dispatch(deleteTrackSuccess(response.data));
        } catch (e) {
            dispatch(deleteTrackFailure(e));
        }
    };
};