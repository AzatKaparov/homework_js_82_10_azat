import {
    CREATE_ARTIST_FAILURE,
    CREATE_ARTIST_REQUEST,
    CREATE_ARTIST_SUCCESS,
    DELETE_ARTIST_FAILURE,
    DELETE_ARTIST_REQUEST,
    DELETE_ARTIST_SUCCESS,
    FETCH_ARTIST_FAILURE,
    FETCH_ARTIST_REQUEST,
    FETCH_ARTIST_SUCCESS,
    FETCH_ARTISTS_FAILURE,
    FETCH_ARTISTS_REQUEST,
    FETCH_ARTISTS_SUCCESS, PUBLISH_ARTIST_FAILURE, PUBLISH_ARTIST_REQUEST, PUBLISH_ARTIST_SUCCESS
} from "../actions/artistsActions";

const initialState = {
    artists: [],
    artistLoading: false,
    error: null,
    currentArtist: null,
};


const artistsReducer = (state=initialState, action) => {
    switch (action.type) {
        case FETCH_ARTISTS_REQUEST:
            return {...state, artistLoading: true};
        case FETCH_ARTISTS_SUCCESS:
            return {...state, artists: action.payload, artistLoading: false, error: null};
        case FETCH_ARTISTS_FAILURE:
            return {...state, error: action.payload, artistLoading: false, artists: []};
        case FETCH_ARTIST_REQUEST:
            return {...state, artistLoading: true};
        case FETCH_ARTIST_SUCCESS:
            return {...state, currentArtist: action.payload, artistLoading: false, error: null};
        case FETCH_ARTIST_FAILURE:
            return {...state, error: action.payload, artistLoading: false, currentArtist: null};
        case CREATE_ARTIST_REQUEST:
            return {...state, artistLoading: true};
        case CREATE_ARTIST_SUCCESS:
            return {...state, artistLoading: false, artists: [...state.artists, action.payload]};
        case CREATE_ARTIST_FAILURE:
            return {...state, artistLoading: false, error: action.payload};
        case DELETE_ARTIST_REQUEST:
            return {...state, artistLoading: true};
        case DELETE_ARTIST_SUCCESS:
            const filtered = state.artists.filter(item => item._id !== action.payload._id);
            return {...state, artistLoading: false, artists: filtered};
        case DELETE_ARTIST_FAILURE:
            return {...state, artistLoading: false, error: action.payload};
        case PUBLISH_ARTIST_REQUEST:
            return {...state, artistLoading: true};
        case PUBLISH_ARTIST_SUCCESS:
            const changed = state.artists.map(item => {
                if (item._id === action.payload._id) {
                    return {
                        ...item,
                        published: true
                    }
                }
                return item;
            });
            return {...state, artistLoading: false, artists: changed};
        case PUBLISH_ARTIST_FAILURE:
            return {...state, artistLoading: false, error: action.payload};
        default:
            return state;
    }
};

export default artistsReducer;