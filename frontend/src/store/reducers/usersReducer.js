import {
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS, LOGOUT_USER,
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS
} from "../actions/usersActions";

const initialState = {
    registerError: null,
    loginError: null,
    user: null,
};

const usersReducer = (state=initialState, action) => {
    switch (action.type) {
        case REGISTER_USER_FAILURE:
            return {...state, registerError: action.payload};
        case REGISTER_USER_SUCCESS:
            return {...state, user: action.payload, registerError: null};
        case LOGIN_USER_SUCCESS:
            return {...state, loginError: null, loginLoading: false, user: action.payload};
        case LOGIN_USER_FAILURE:
            return {...state, loginError: action.payload};
        case LOGOUT_USER:
            return {...state, user: null};
        default:
            return state;
    }
};

export default usersReducer;