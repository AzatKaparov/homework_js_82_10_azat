import {
    DELETE_ALBUM_FAILURE,
    DELETE_ALBUM_REQUEST,
    DELETE_ALBUM_SUCCESS,
    FETCH_ALBUM_FAILURE,
    FETCH_ALBUM_REQUEST,
    FETCH_ALBUM_SUCCESS,
    FETCH_ALBUMS_FAILURE,
    FETCH_ALBUMS_REQUEST,
    FETCH_ALBUMS_SUCCESS
} from "../actions/albumsActions";

const initialState = {
    albumLoading: false,
    error: null,
    albums: [],
    currentAlbum: null,
};

const albumsReducer = (state=initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUMS_REQUEST:
            return {...state, albumLoading: true};
        case FETCH_ALBUMS_SUCCESS:
            return {...state, albums: action.payload, albumLoading: false, error: null};
        case FETCH_ALBUMS_FAILURE:
            return {...state, error: action.payload, albumLoading: false, albums: []};
        case FETCH_ALBUM_REQUEST:
            return {...state, albumLoading: true};
        case FETCH_ALBUM_SUCCESS:
            return {...state, currentAlbum: action.payload, albumLoading: false, error: null};
        case FETCH_ALBUM_FAILURE:
            return {...state, error: action.payload, albumLoading: false, currentAlbum: null};
        case DELETE_ALBUM_REQUEST:
            return {...state, albumLoading: true};
        case DELETE_ALBUM_SUCCESS:
            const filtered = state.albums.filter(item => item._id !== action.payload._id);
            return {...state, albumLoading: false, albums: filtered};
        case DELETE_ALBUM_FAILURE:
            return {...state, albumLoading: false, error: action.payload};
        default:
            return state;
    }
};

export default  albumsReducer;