import {
    CREATE_TRACK_HISTORY_FAILURE,
    CREATE_TRACK_HISTORY_REQUEST,
    CREATE_TRACK_HISTORY_SUCCESS,
    DELETE_TRACK_FAILURE,
    DELETE_TRACK_REQUEST,
    DELETE_TRACK_SUCCESS,
    FETCH_TRACK_HISTORY_FAILURE,
    FETCH_TRACK_HISTORY_REQUEST,
    FETCH_TRACK_HISTORY_SUCCESS,
    FETCH_TRACKS_FAILURE,
    FETCH_TRACKS_REQUEST,
    FETCH_TRACKS_SUCCESS
} from "../actions/tracksActions";
import {CREATE_ALBUM_FAILURE, CREATE_ALBUM_REQUEST, CREATE_ALBUM_SUCCESS} from "../actions/albumsActions";

const initialState = {
    trackLoading: false,
    error: null,
    tracks: [],
    historyTracks: [],
};

const tracksReducer = (state=initialState, action) => {
    switch (action.type) {
        case FETCH_TRACKS_REQUEST:
            return {...state, trackLoading: true, error: null};
        case FETCH_TRACKS_SUCCESS:
            return {...state, tracks: action.payload, trackLoading: false, error: null};
        case FETCH_TRACKS_FAILURE:
            return {...state, error: action.payload, trackLoading: false, tracks: []};
        case CREATE_TRACK_HISTORY_REQUEST:
            return {...state, trackLoading: true, error: null};
        case CREATE_TRACK_HISTORY_SUCCESS:
            return {...state, trackLoading: false, error: null};
        case CREATE_TRACK_HISTORY_FAILURE:
            return {...state, trackLoading: false, error: action.payload};
        case FETCH_TRACK_HISTORY_REQUEST:
            return {...state, trackLoading: true, error: null};
        case FETCH_TRACK_HISTORY_SUCCESS:
            return {...state, historyTracks: action.payload, trackLoading: false, error: null};
        case FETCH_TRACK_HISTORY_FAILURE:
            return {...state, error: action.payload, trackLoading: false, historyTracks: []};
        case CREATE_ALBUM_REQUEST:
            return {...state, trackLoading: true};
        case CREATE_ALBUM_SUCCESS:
            return {...state, trackLoading: false};
        case CREATE_ALBUM_FAILURE:
            return {...state, trackLoading: false, error: action.payload};
        case DELETE_TRACK_REQUEST:
            return {...state, trackLoading: true};
        case DELETE_TRACK_SUCCESS:
            const filtered = state.tracks.filter(item => item._id !== action.payload._id);
            return {...state, trackLoading: false, tracks: filtered};
        case DELETE_TRACK_FAILURE:
            return {...state, trackLoading: false, error: action.payload};
        default:
            return state;
    }
};

export default  tracksReducer;