import React from 'react';
import './TrackItem.css';
import {useDispatch, useSelector} from "react-redux";
import {createTrackHistory, deleteTrack} from "../../store/actions/tracksActions";
import {Button} from "react-bootstrap";

const TrackItem = ({ name, id, duration, number, published }) => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    return (
        <div className="TrackItem p-2">
            <p className="mr-2">{number}</p>
            <p className="mr-5">
                {name}
                {!published && <span className="text-danger ml-4">Unpublished</span> }
            </p>
            <p className="ml-auto">{duration}</p>
            {user
                ? <Button onClick={() => dispatch(createTrackHistory(id))} className="mx-3">Play</Button>
                : null
            }
            {user?.role === "admin" &&
                <Button onClick={() => dispatch(deleteTrack(id))} variant="danger" className="text-white mr-3" to={``}>Delete</Button>
            }
            {user?.role === "admin" && !published &&
                <Button variant="warning">Publish</Button>
            }
        </div>
    );
};

export default TrackItem;