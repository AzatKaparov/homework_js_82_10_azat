import React from 'react';
import {transformDate} from "../../constants";

const HistoryItem = ({track, artist, date, album}) => {
    return (
        <div className="d-flex justify-content-between">
            <p>{track}</p>
            <p>{album}</p>
            <p>{artist}</p>
            <p>{transformDate(date)}</p>
        </div>
    );
};

export default HistoryItem;