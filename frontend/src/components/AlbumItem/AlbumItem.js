import React from 'react';
import './AlbumItem.css';
import {API_URL, transformDate} from "../../constants";
import {NavLink} from "react-router-dom";
import {Button} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {deleteAlbum} from "../../store/actions/albumsActions";

const AlbumItem = ({id, name, image, date, published}) => {
    const dispatch = useDispatch();
    const { user } = useSelector(state => state.users);

    return (
        <div className="AlbumItem mx-3 pb-3">
            <div className="AlbumImgBlock">
                <img src={`${API_URL}/${image}`} alt={name}/>
            </div>
            <div className="AlbumTextBlock p-2 d-flex flex-column justify-content-between h-100 text-left">
                {!published && <p className="text-danger m-0">Unpublished</p>}
                <ul className="p-0">
                    <li>"<b>{name}</b>"</li>
                    <li>{transformDate(date)}</li>
                </ul>
                <div>
                    <NavLink className="btn bg-dark text-white mr-3" to={`/tracks?album=${id}`}>More</NavLink>
                    {user?.role === "admin" &&
                        <Button onClick={() => dispatch(deleteAlbum(id))} variant="danger" className="text-white mr-3" to={``}>Delete</Button>
                    }
                    {user?.role === "admin" && !published &&
                        <Button variant="warning">Publish</Button>
                    }
                </div>
            </div>
        </div>
    );
};

export default AlbumItem;