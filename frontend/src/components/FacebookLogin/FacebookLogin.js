import React from 'react';
import {useDispatch} from "react-redux";
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import {Button} from "react-bootstrap";
import {facebookLogin} from "../../store/actions/usersActions";
import {facebookAppId} from "../../constants";

const FacebookLogin = () => {
    const dispatch = useDispatch();

    const responseFacebook = response => {
        dispatch(facebookLogin(response));
    };

    return (
        <FacebookLoginButton
            appId={facebookAppId}
            fields="name,email,picture"
            render={props => (
                <Button
                    variant="primary"
                    onClick={props.onClick}
                >
                    Login with facebook
                </Button>
            )}
            callback={responseFacebook} />
    );
};

export default FacebookLogin;