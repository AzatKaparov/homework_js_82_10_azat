import React from 'react';
import {Form} from "react-bootstrap";
import PropTypes from 'prop-types';

const FormElement = ({ onChangeHandler, name, value, type, placeholder, required, label }) => {
    return (
        <Form.Group className="mb-3">
            <Form.Label>{label}</Form.Label>
            <Form.Control
                onChange={onChangeHandler}
                name={name}
                value={value ? value : ""}
                required={required}
                type={type}
                placeholder={placeholder}
            />
        </Form.Group>
    );
};

FormElement.propTypes = {
    onChangeHandler: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.string,
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    required: PropTypes.bool,
    label: PropTypes.string.isRequired
};

export default FormElement;