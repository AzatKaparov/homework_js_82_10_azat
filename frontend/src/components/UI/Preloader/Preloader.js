import React from 'react';
import './Preloader.css';

const Preloader = ({show}) => {
    return show
        ? <div className="Preloader"/>
        : null;
};

export default Preloader;