import React from 'react';
import NavItem from "../NavItem/NavItem";
import {useDispatch, useSelector} from "react-redux";
import {logoutUser} from "../../../store/actions/usersActions";
import {Button} from "react-bootstrap";

const UserMenu = () => {
    const dispatch = useDispatch();
    const { user } = useSelector(state => state.users);
    const logout = () => {dispatch(logoutUser())};

    return (
        <>
            <NavItem to="track_history" title="Track History"/>
            <NavItem to="/artists/add" title="Add artist"/>
            <NavItem to="/albums/add" title="Add album"/>
            <NavItem to="/tracks/add" title="Add track"/>
            <Button onClick={logout} variant="warning">Logout</Button>
            <div className="d-flex align-items-center ml-auto">
                <p className="text-white my-0 mx-2">{user?.displayName}</p>
                <img width="50" height="50" src={user?.avatarImage} alt={user?.displayName}/>
            </div>
        </>
    );
};

export default UserMenu;