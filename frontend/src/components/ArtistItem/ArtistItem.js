import React from 'react';
import {API_URL} from "../../constants";
import './ArtistItem.css';
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {Button} from "react-bootstrap";
import {deleteArtist, publishArtist} from "../../store/actions/artistsActions";

const ArtistItem = ({name, photo, id, published}) => {
    const dispatch = useDispatch();
    const { user } = useSelector(state => state.users);

    return (
        <div className="ArtistItem mb-4">
            <div className="img-block">
                <img src={`${API_URL}/${photo}`} alt=""/>
            </div>
            <div className="text-block p-3">
                {!published && user?.role === "admin" &&
                    <div className="unpub-block d-flex flex-column">
                        <p className="text-danger my-1">Unpublished</p>
                        <Button onClick={() => dispatch(publishArtist(id))} variant="warning" className="btn-sm">Publish</Button>
                    </div>
                }
                <h1>{name}</h1>
                <div>
                    <NavLink className="btn bg-dark text-white mr-3" to={`/albums?artist=${id}`}>More...</NavLink>
                    {user?.role === "admin" &&
                        <Button onClick={() => dispatch(deleteArtist(id))} variant="danger" className="text-white mr-3" to={``}>Delete</Button>
                    }
                </div>

            </div>
        </div>
    );
};

export default ArtistItem;