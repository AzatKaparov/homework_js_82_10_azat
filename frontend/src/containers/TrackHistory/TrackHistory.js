import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchTrackHistory} from "../../store/actions/tracksActions";
import HistoryItem from "../../components/HistoryItem/HistoryItem";
import {Redirect} from "react-router-dom";

const TrackHistory = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const history = useSelector(state => state.tracks.historyTracks);

    useEffect(() => {
        dispatch(fetchTrackHistory());
    }, [dispatch]);

    if (!user) {
        return <Redirect to="/login"/>
    }

    return (
        <div className="container">
            {history.length > 0
                ? history.map(item => (
                    <HistoryItem
                        key={item._id}
                        track={item.track.name}
                        artist={item.track.album.artist.name}
                        date={item.datetime}
                        album={item.track.album.name}
                    />
                ))
                : <h2>There is no tracks in your history</h2>
            }
        </div>
    );
};

export default TrackHistory;