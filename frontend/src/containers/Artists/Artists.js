import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import ArtistItem from "../../components/ArtistItem/ArtistItem";
import {fetchArtists} from "../../store/actions/artistsActions";
import Preloader from "../../components/UI/Preloader/Preloader";
import Backdrop from "../../components/UI/Backdrop/Backdrop";
import Notification from "../../components/UI/Notification/Notification";

const Artists = () => {
    const dispatch = useDispatch();
    const artistError = useSelector(state => state.artists.error);
    const {artists, artistLoading} = useSelector(state => state.artists);
    const { user } = useSelector(state => state.users);

    let errorNotif;
    if (artistError) {
        errorNotif = <Notification show={!!artistError} text={artistError.message} type="Error" />;
    } else {
        errorNotif = null;
    }

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch, user]);

    return (
        <>
            <Preloader show={artistLoading} />
            <Backdrop show={artistLoading} />
            <div className="container">
                {errorNotif}
                {user?.role === "admin"
                    ? artists.map(artist => (
                    <ArtistItem
                        key={artist._id}
                        photo={artist.photo}
                        name={artist.name}
                        id={artist._id}
                        published={artist.published}
                    />))
                    : artists
                        .filter(artist => artist.published === true)
                        .map(artist => (
                            <ArtistItem
                                key={artist._id}
                                photo={artist.photo}
                                name={artist.name}
                                id={artist._id}
                                published={artist.published}
                            />))
                }
            </div>
        </>
    );
};

export default Artists;