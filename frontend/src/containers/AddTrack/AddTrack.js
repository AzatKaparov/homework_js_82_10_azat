import React, {useEffect, useState} from 'react';
import {Button, Form} from 'react-bootstrap';
import FormElement from "../../components/UI/FormElement/FormElement";
import {useDispatch, useSelector} from "react-redux";
import Backdrop from "../../components/UI/Backdrop/Backdrop";
import Preloader from "../../components/UI/Preloader/Preloader";
import {fetchAlbums} from "../../store/actions/albumsActions";
import {createTrack} from "../../store/actions/tracksActions";

const AddTrack = () => {
    const dispatch = useDispatch();
    const { error, trackLoading } = useSelector(state => state.tracks);
    const { albums } = useSelector(state => state.albums);
    const [track, setTrack] = useState({
        name: "",
        album: "",
        duration: "",
        trackNumber: ""
    });

    console.log(track);

    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setTrack(prevState => ({...prevState, [name]: value}));
    };

    const onFormSubmit = async e => {
        e.preventDefault();
        await dispatch(createTrack(track));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    useEffect(() => {
        dispatch(fetchAlbums());
    }, [dispatch]);

    return (
        <>
            <Preloader show={trackLoading} />
            <Backdrop show={trackLoading} />
            <div className="container">
                <Form onSubmit={onFormSubmit}>
                    <FormElement
                        value={track.name}
                        label="Name"
                        required={true}
                        name="name"
                        onChangeHandler={inputChangeHandler}
                        type="text"
                        error={getFieldError("name")}
                    />
                    <FormElement
                        value={track.duration}
                        label="Duration"
                        required={true}
                        name="duration"
                        onChangeHandler={inputChangeHandler}
                        type="text"
                        placeholder="01:12 - like that"
                        error={getFieldError("duration")}
                    />
                    <FormElement
                        value={track.trackNumber}
                        label="Track number"
                        required={true}
                        name="trackNumber"
                        onChangeHandler={inputChangeHandler}
                        type="number"
                        error={getFieldError("trackNumber")}
                    />
                    <Form.Label className="d-block">Album</Form.Label>
                    <Form.Select name="album" className="p-2 d-block" onChange={inputChangeHandler}>
                        <option>Please select an album</option>
                        {albums.map(item => (
                            <option key={item._id} value={item._id}>{item.name}</option>
                        ))}
                    </Form.Select>
                    <Button className="my-3" type="submit" variant="primary">Submit</Button>
                </Form>
            </div>
        </>
    );
};

export default AddTrack;