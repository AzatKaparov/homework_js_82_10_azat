import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {API_URL} from "../../constants";
import './Tracks.css';
import TrackItem from "../../components/TrackItem/TrackItem";
import {fetchAlbum} from "../../store/actions/albumsActions";
import {fetchTracks} from "../../store/actions/tracksActions";
import queryString from "query-string";
import Preloader from "../../components/UI/Preloader/Preloader";
import Backdrop from "../../components/UI/Backdrop/Backdrop";
import Notification from "../../components/UI/Notification/Notification";

const Tracks = ({location}) => {
    const dispatch = useDispatch();
    const params = queryString.parse(location.search);
    const {currentAlbum, albumLoading} = useSelector(state => state.albums);
    const {tracks, trackLoading} = useSelector(state => state.tracks);
    const trackError = useSelector(state => state.tracks.error);
    const albumError = useSelector(state => state.albums.error);
    const {user} = useSelector(state => state.users);

    let errorNotif;
    if (trackError) {
        errorNotif = <Notification show={!!trackError} text={trackError.message} type="Error" />;
    } else if (albumError) {
        errorNotif = <Notification show={!!albumError} text={albumError.message} type="Error" />;
    } else {
        errorNotif = null;
    }

    useEffect(() => {
        if (params.album) {
            dispatch(fetchAlbum(params.album));
            dispatch(fetchTracks(params.album));
        }
    }, [dispatch, params.album]);


    return currentAlbum && (
        <>
            <Preloader show={trackLoading || albumLoading} />
            <Backdrop show={trackLoading || albumLoading} />
            <div className="container">
                {errorNotif}
                <div className="AlbumInfo">
                    <div className="AlbumImg">
                        <img src={`${API_URL}/${currentAlbum.image}`} alt=""/>
                    </div>
                    <h1 className="my-3 font-weight-bold">{currentAlbum.name}</h1>
                    <h2>{currentAlbum.artist.name}</h2>
                </div>
                <div className="track-block">
                    {user?.role === 'admin'
                        ? tracks.map(track => (
                            <TrackItem
                                key={track._id}
                                name={track.name}
                                duration={track.duration}
                                number={track.trackNumber}
                                id={track._id}
                                published={track.published}
                            />
                        ))
                        : tracks.filter(track => track.published === true)
                            .map(track => (
                            <TrackItem
                                key={track._id}
                                name={track.name}
                                duration={track.duration}
                                number={track.trackNumber}
                                id={track._id}
                                published={track.published}
                            />
                        ))}
                    {user?.role !== "admin" && tracks.filter(item => item.published === true).length === 0 &&  <h2 className="text-info">There is no tracks in that album</h2>}
                    {user?.role === "admin" && tracks.length <= 0 && <h2 className="text-info">There is no tracks in that album</h2>}
                </div>
            </div>
        </>
    );
};

export default Tracks;