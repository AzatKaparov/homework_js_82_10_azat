import React, {useState} from 'react';
import {Button, Form} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {registerUser} from "../../store/actions/usersActions";
import Notification from "../../components/UI/Notification/Notification";
import FormElement from "../../components/UI/FormElement/FormElement";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

const Register = () => {
    const dispatch = useDispatch();
    const registerError = useSelector(state => state.users.registerError);
    const [user, setUser] = useState({
        username: "",
        password: "",
    });

    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setUser(prevState => ({...prevState, [name]: value}));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(registerUser({...user}));
    };

    let errorNotif;
    if (registerError) {
        errorNotif = <Notification show={!!registerError} text={registerError.message} type="Error" />;
    } else {
        errorNotif = null;
    }

    return (
        <div className="container">
            {errorNotif}
            <Form onSubmit={submitFormHandler}>
                <FormElement
                    onChangeHandler={inputChangeHandler}
                    name="username"
                    type="text"
                    value={user.username}
                    placeholder="Username"
                    required={true}
                    label="Username"
                />
                <FormElement
                    onChangeHandler={inputChangeHandler}
                    name="password"
                    value={user.password}
                    type="password"
                    placeholder="Password"
                    required={true}
                    label="Password"
                />
                <div className="btns">
                    <Button className="mb-2" variant="primary" type="submit">
                        Submit
                    </Button>
                    <br/>
                    <FacebookLogin />
                </div>
            </Form>
        </div>
    );
};

export default Register;