import React, {useEffect, useState} from 'react';
import {Button, Form} from 'react-bootstrap';
import FormElement from "../../components/UI/FormElement/FormElement";
import {useDispatch, useSelector} from "react-redux";
import Backdrop from "../../components/UI/Backdrop/Backdrop";
import Preloader from "../../components/UI/Preloader/Preloader";
import {fetchArtists} from "../../store/actions/artistsActions";
import {createAlbum} from "../../store/actions/albumsActions";

const AddAlbum = () => {
    const dispatch = useDispatch();
    const { error, albumLoading } = useSelector(state => state.albums);
    const { artists } = useSelector(state => state.artists);
    const [album, setAlbum] = useState({
        name: "",
        image: "",
        artist: "",
    });

    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setAlbum(prevState => ({...prevState, [name]: value}));
    };

    const onFileChange = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setAlbum(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const onFormSubmit = async albumData => {
        await dispatch(createAlbum(albumData));
    };

    const submitFormHandler  = e => {
        e.preventDefault();
        const formData = new FormData();

        Object.keys(album).forEach(key => {
            formData.append(key, album[key]);
        });
        onFormSubmit(formData).catch(err => console.error(err));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);

    return (
        <>
            <Preloader show={albumLoading} />
            <Backdrop show={albumLoading} />
            <div className="container">
                <Form onSubmit={submitFormHandler}>
                    <FormElement
                        value={album.name}
                        label="Name"
                        required={true}
                        name="name"
                        onChangeHandler={inputChangeHandler}
                        type="text"
                        error={getFieldError("name")}
                    />
                    <Form.Group>
                        <Form.Label>Image</Form.Label>
                        <Form.Control type="file" name="image" required={true} onChange={onFileChange} />
                        {error ? <p>{getFieldError("image")}</p> : null}
                    </Form.Group>
                    <Form.Label className="d-block">Artist</Form.Label>
                    <Form.Select name="artist" className="p-2 d-block" onChange={inputChangeHandler}>
                        <option>Please select an artist</option>
                        {artists.map(item => (
                            <option key={item._id} value={item._id}>{item.name}</option>
                        ))}
                    </Form.Select>
                    <Button className="my-3" type="submit" variant="primary">Submit</Button>
                </Form>
            </div>
        </>
    );
};

export default AddAlbum;