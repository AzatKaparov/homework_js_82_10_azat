import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Button, Form} from "react-bootstrap";
import FormElement from "../../components/UI/FormElement/FormElement";
import {loginUser} from "../../store/actions/usersActions";
import {NavLink} from "react-router-dom";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

const Login = () => {
    const dispatch = useDispatch();
    const {loginError} = useSelector(state => state.users);
    const [user, setUser] = useState({
        username: "",
        password: "",
    });


    const inputChangeHandler = e => {
        const {name, value} = e.target;

        setUser(prevState => ({...prevState, [name]: value}));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(loginUser({...user}));
    };

    const getFieldError = fieldName => {
        try {
            return loginError.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <div className="container">
            <Form onSubmit={submitFormHandler}>
                <FormElement
                    value={user.username}
                    name="username"
                    onChangeHandler={inputChangeHandler}
                    type="text"
                    required={true}
                    placeholder="Username"
                    label="Username"
                    error={loginError ? loginError.message : null}
                />
                <FormElement
                    value={user.password}
                    name="password"
                    onChangeHandler={inputChangeHandler}
                    type="password"
                    required={true}
                    placeholder="Password"
                    label="Password"
                    error={loginError ? loginError.message : null}
                />
                <Form.Text>
                    Don't have an account?
                    <NavLink to="/register">Register now</NavLink>
                </Form.Text>
                <Button className="mb-3" type="submit" variant="primary">
                    Submit
                </Button>
                <br/>
                <FacebookLogin />
            </Form>
        </div>
    );
};

export default Login;