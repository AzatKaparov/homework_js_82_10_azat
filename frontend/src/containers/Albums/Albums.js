import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {API_URL} from "../../constants";
import './Albums.css';
import AlbumItem from "../../components/AlbumItem/AlbumItem";
import queryString from 'query-string';
import {fetchArtist} from "../../store/actions/artistsActions";
import {fetchAlbums} from "../../store/actions/albumsActions";
import Backdrop from "../../components/UI/Backdrop/Backdrop";
import Preloader from "../../components/UI/Preloader/Preloader";
import Notification from "../../components/UI/Notification/Notification";

const Albums = ({location}) => {
    const dispatch = useDispatch();
    const params = queryString.parse(location.search);
    const {currentArtist, artistLoading} = useSelector(state => state.artists);
    const {albums, albumLoading} = useSelector(state => state.albums);
    const artistError = useSelector(state => state.artists.error);
    const albumError = useSelector(state => state.albums.error);
    const { user } = useSelector(state => state.users);

    let errorNotif;
    if (artistError) {
        errorNotif = <Notification show={!!artistError} text={artistError.message} type="Error" />;
    } else if (albumError) {
        errorNotif = <Notification show={!!albumError} text={albumError.message} type="Error" />;
    } else {
        errorNotif = null;
    }

    useEffect(() => {
        if (params.artist) {
            dispatch(fetchArtist(params.artist));
            dispatch(fetchAlbums(params.artist));
        }
    }, [dispatch, params.artist]);

    if (currentArtist === null) {
        return (<h1>Not found</h1>);
    } else if (user?.role !== "admin" && currentArtist.published === false) {
        return (<h1>Not found</h1>);
    }

    return currentArtist && (
        <>
            <Preloader show={artistLoading || albumLoading} />
            <Backdrop show={artistLoading || albumLoading}/>
            <div className="container pb-5">
                {errorNotif}
                <div className="artist-info">
                    <div className="ArtistImgBlock">
                        <img src={`${API_URL}/${currentArtist.photo}`} alt=""/>
                    </div>
                    <div className="ArtistTextBlock">
                        <h1>{currentArtist.name}</h1>
                        <p>{currentArtist.info}</p>
                    </div>
                    <h2 className="my-5">Albums</h2>
                    <div className="AlbumsBlock">
                        {user?.role === "admin"
                            ? albums.map(item => (
                                <AlbumItem
                                    key={item._id}
                                    id={item._id}
                                    image={item.image}
                                    name={item.name}
                                    date={item.releaseDate}
                                    published={item.published}
                                />
                            ))
                            : albums.filter(item => item.published === true)
                                .map(item => (
                                <AlbumItem
                                    key={item._id}
                                    id={item._id}
                                    image={item.image}
                                    name={item.name}
                                    date={item.releaseDate}
                                    published={item.published}
                                />
                            ))}
                        {albums.length <= 0 && <h2>There is no any albums here</h2>}
                    </div>
                </div>
            </div>
        </>
    );
};

export default Albums;