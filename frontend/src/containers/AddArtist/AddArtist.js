import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Button, Form} from 'react-bootstrap';
import FormElement from "../../components/UI/FormElement/FormElement";
import {createArtist} from "../../store/actions/artistsActions";
import Backdrop from "../../components/UI/Backdrop/Backdrop";
import Preloader from "../../components/UI/Preloader/Preloader";


const AddArtist = () => {
    const dispatch = useDispatch();
    const { error, artistLoading } = useSelector(state => state.artists);
    const [artist, setArtist] = useState({
        name: "",
        info: "",
        photo: "",
    });

    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setArtist(prevState => ({...prevState, [name]: value}));
    };

    const onFileChange = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setArtist(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const onFormSubmit = async artistData => {
        await dispatch(createArtist(artistData));
    };

    const submitFormHandler  = e => {
        e.preventDefault();
        const formData = new FormData();

        Object.keys(artist).forEach(key => {
            formData.append(key, artist[key]);
        });
        onFormSubmit(formData).catch(err => console.error(err));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <>
            <Preloader show={artistLoading} />
            <Backdrop show={artistLoading} />
            <div className="container">
                <Form onSubmit={submitFormHandler}>
                    <FormElement
                        value={artist.name}
                        label="Name"
                        required={true}
                        name="name"
                        onChangeHandler={inputChangeHandler}
                        type="text"
                        error={getFieldError("name")}
                    />
                    <FormElement
                        value={artist.info}
                        label="Info"
                        as="textarea"
                        required={true}
                        name="info"
                        onChangeHandler={inputChangeHandler}
                        type="text"
                        error={getFieldError("info")}
                    />
                    <Form.Group>
                        <Form.Label>Image</Form.Label>
                        <Form.Control type="file" name="photo" required={true} onChange={onFileChange} />
                        {error ? <p>{getFieldError("photo")}</p> : null}
                    </Form.Group>
                    <Button className="my-3" type="submit" variant="primary">Submit</Button>
                </Form>
            </div>
        </>
    );
};

export default AddArtist;